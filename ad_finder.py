import abc
import csv
import configparser
import win32api, win32con
from time import time, sleep
from PIL import ImageGrab
from requests import Request
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary
from win32api import GetSystemMetrics
import logging
import urllib
import queue
import sys
import encodings
# from selenium.webdriver.common.keys import Keys
# from bs4 import BeautifulSoup

# прочтем конфиги
config = configparser.ConfigParser()
config.read('config.ini')
ad_fnd_conf = config['AD_FINDER']

YA_HEAD_SIZE = int(ad_fnd_conf['YA_HEAD_SIZE'])
GO_HEAD_SIZE = int(ad_fnd_conf['GO_HEAD_SIZE'])

DATE_X_OFFSET = int(ad_fnd_conf['DATE_X_OFFSET'])
DATE_Y_OFFSET = int(ad_fnd_conf['DATE_Y_OFFSET'])

SCREEN_X_COORD = int(ad_fnd_conf['SCREEN_X_COORD'])
SCREEN_Y_COORD = int(ad_fnd_conf['SCREEN_Y_COORD'])

YA_CSV_FILE_NAME = ad_fnd_conf['YA_CSV_FILE_NAME']
GO_CSV_FILE_NAME = ad_fnd_conf['GO_CSV_FILE_NAME']
LOG_FILE_NAME = ad_fnd_conf['LOG_FILE_NAME']

YA_USER = ad_fnd_conf['YA_USER']
YA_KEY = ad_fnd_conf['YA_KEY']

FF_DRIVER_PATH = ad_fnd_conf['FF_DRIVER_PATH']
EDGE_DRIVER_PATH = ad_fnd_conf['EDGE_DRIVER_PATH']
CHROME_DRIVER_PATH = ad_fnd_conf['CHROME_DRIVER_PATH']
OPERA_DRIVER_PATH = ad_fnd_conf['OPERA_DRIVER_PATH']
SELECTED_DRIVER = ad_fnd_conf['SELECTED_DRIVER']

FF_BROWSER_PATH = ad_fnd_conf['FF_BROWSER_PATH']
FF_CAPS_M_VAL = ad_fnd_conf['FF_CAPS_M_VAL']

# Настроим формат вывода логов, пока что в консоль
logging.basicConfig(
    format='[%(filename)s: 'u'%(lineno)d, %(asctime)s] %(levelname)s: %(message)s',
    level=logging.INFO,
    filename=LOG_FILE_NAME)


class AdFinder(object):
    __metaclass__ = abc.ABCMeta

    @abc.abstractmethod
    def __init__(self, title: str = None, key_word: str = None):
        self._key_word = key_word
        self._title = title
        self._url = None
        self._params = dict()
        self._offset = None
        self._class_title = None
        self.driver = None

    @abc.abstractmethod
    def _get_driver(self):
        if SELECTED_DRIVER == 'FF':
            if FF_BROWSER_PATH == 'NOT_SET':
                return webdriver.Firefox(executable_path=FF_DRIVER_PATH)
            else:
                ff_binary = FirefoxBinary(FF_BROWSER_PATH)
                ff_caps = webdriver.DesiredCapabilities().FIREFOX
                ff_caps["marionette"] = FF_CAPS_M_VAL
                return webdriver.Firefox(executable_path=FF_DRIVER_PATH,
                                         firefox_binary=ff_binary,
                                         capabilities=ff_caps
                                         )
        elif SELECTED_DRIVER == 'CHROME':
            return webdriver.Chrome(executable_path=CHROME_DRIVER_PATH)
        elif SELECTED_DRIVER == 'EDGE':
            return webdriver.Edge(executable_path=EDGE_DRIVER_PATH)
        elif SELECTED_DRIVER == 'OPERA':
            return webdriver.Opera(executable_path=OPERA_DRIVER_PATH)
        else:
            raise TypeError('No such webdriver.')

    @abc.abstractmethod
    def _click_to_date(self):
        # получим разрешение экрана
        scr_x = GetSystemMetrics(SCREEN_X_COORD)
        scr_y = GetSystemMetrics(SCREEN_Y_COORD)
        # пытаемся попасть по дате в трее
        date_x = scr_x - DATE_X_OFFSET
        date_y = scr_y - DATE_Y_OFFSET
        win32api.SetCursorPos((date_x, date_y))
        win32api.mouse_event(win32con.MOUSEEVENTF_LEFTDOWN, date_x, date_y, 0, 0)
        win32api.mouse_event(win32con.MOUSEEVENTF_LEFTUP, date_x, date_y, 0, 0)
        sleep(0.5)

    @abc.abstractmethod
    def _make_screenshot(self, ad_elem):
        # доскроллим до нужного блока
        self.driver.execute_script("arguments[0].scrollIntoView();", ad_elem)
        # отъедем чуть вверх, чтоб шапка элемент не прятала
        self.driver.execute_script("window.scrollBy(0, {});".format(-self._offset))
        sleep(0.75)
        screen = ImageGrab.grab()
        screen.save('screenshots/{0}_{1}_{2}.png'.format(self._class_title,
                                                         self._key_word.replace(' ', '_'),
                                                         str(time())), 'PNG')

    @abc.abstractmethod
    def set_attrs(self, key_word, title):
        pass

    @abc.abstractmethod
    def _get_page(self):
        r = Request('GET', self._url, params=self._params).prepare()
        logging.info('Trying to get url: {}'.format(r.url))
        self.driver.get(r.url)
        html_src = self.driver.page_source
        if html_src:
            return html_src
        return None

    @abc.abstractmethod
    def find_ad(self):
        self.driver = self._get_driver()
        html_src = self._get_page()

        if not html_src:
            logging.error("Can't get page sources.")
            return

        try:
            ad_elem = self.driver.find_element_by_partial_link_text(self._title)
            self._click_to_date()
            self._make_screenshot(ad_elem)
            self._click_to_date()
        except NoSuchElementException as e:
            logging.error('There no such ad title "{}" in the page.'.format(self._title))
            return
        finally:
            self.driver.close()
        return 1


class YandexAdFinder(AdFinder):
    def __init__(self, title=None, key_word=None, ad_cnt=8,
                 ad_class='link organic__url i-bem',
                 url='https://yandex.ru/search'):
        super().__init__()
        self._key_word = key_word
        self._title = title
        # self._ad_cnt = ad_cnt
        # self._ad_class = ad_class
        self._url = url
        self._params = {'text': key_word,
                        'user': YA_USER,
                        'key': YA_KEY}
        self._offset = YA_HEAD_SIZE
        self._class_title = 'YA'

    def set_attrs(self, key_word, title):
        self._key_word = key_word
        self._title = title
        self._params['text'] = key_word

    # def _get_page(self):
    #     r = Request('GET', self._url, params=self._params).prepare()
    #     logging.info('Trying to get url: {}'.format(r.url))
    #     self.driver.get(r.url)
    #     html_src = self.driver.page_source
    #     if html_src:
    #         return html_src
    #     return None
    #
    # def _click_to_date(self):
    #     # получим разрешение экрана
    #     scr_x = GetSystemMetrics(SCREEN_X_COORD)
    #     scr_y = GetSystemMetrics(SCREEN_Y_COORD)
    #     # пытаемся попасть по дате в трее
    #     date_x = scr_x - DATE_X_OFFSET
    #     date_y = scr_y - DATE_Y_OFFSET
    #     win32api.SetCursorPos((date_x, date_y))
    #     win32api.mouse_event(win32con.MOUSEEVENTF_LEFTDOWN, date_x, date_y, 0, 0)
    #     win32api.mouse_event(win32con.MOUSEEVENTF_LEFTUP, date_x, date_y, 0, 0)
    #     sleep(0.5)
    #
    # def _make_screenshot(self, ad_elem):
    #     # доскроллим до нужного блока
    #     self.driver.execute_script("arguments[0].scrollIntoView();", ad_elem)
    #     # отъедем чуть вверх, чтоб шапка элемент не прятала
    #     self.driver.execute_script("window.scrollBy(0, {});".format(-YA_HEAD_SIZE))
    #     sleep(0.75)
    #     screen = ImageGrab.grab()
    #     screen.save('screenshots/{0}_{1}.png'.format(self._key_word.replace(' ', '_'),
    #                                                  str(time())), 'PNG')
    #
    # def set_attrs(self, key_word, title):
    #     self._key_word = key_word
    #     self._title = title
    #     self._params['text'] = key_word
    #
    # def find_ad(self):
    #     self.driver = webdriver.Firefox(executable_path=r'geckodriver.exe')
    #     html_src = self._get_page()
    #
    #     if not html_src:
    #         logging.error("Can't get page sources.")
    #         return
    #
    #     '''
    #     soup = BeautifulSoup(html_src, "lxml")
    #     s = soup.findAll('a', attrs={"class": self._ad_class})
    #     # выкинем все лишнее из результатов поиска
    #     ad_titles = list(link.text for link in s)
    #     if self._title not in ad_titles:
    #         print('Not such ad title in the page')
    #         return
    #     ad_num = ad_titles.index(self._title)
    #     '''
    #
    #     try:
    #         ad_elem = self.driver.find_element_by_partial_link_text(self._title)
    #         self._click_to_date()
    #         self._make_screenshot(ad_elem)
    #         self._click_to_date()
    #     except NoSuchElementException as e:
    #         logging.error('There no such ad title "{}" in the page.'.format(self._title))
    #         return
    #     finally:
    #         self.driver.close()
    #     return 1


class GoogleAdFinder(AdFinder):
    def __init__(self, title=None, key_word=None,
                 url='http://google.com/search'):
        super().__init__()
        self._key_word = key_word
        self._title = title
        self._url = url
        self._params = dict()
        self._offset = GO_HEAD_SIZE
        self._class_title = 'GO'

    def set_attrs(self, key_word, title):
        self._key_word = key_word
        self._title = title
        self._params['q'] = key_word


def read_from_csv(file_name):
    data = list()
    with open(file_name, 'r', encoding="windows-1251") as f:
        csv_reader = csv.reader(f, delimiter=';')
        for row in csv_reader:
            if len(row) > 1:
                data.append((row[0], row[1]))
    return data


def main():
    try:
        go_data = read_from_csv(GO_CSV_FILE_NAME)
        go_data = list(filter(lambda x: x[0], go_data))  # выкинем пустые ключевые слова
    except Exception as e:
        logging.error('Error occurred while reading from google csv file. '
                      'Details: {} '.format(e))
        return 0

    try:
        ya_data = read_from_csv(YA_CSV_FILE_NAME)
        ya_data = list(filter(lambda x: x[0], ya_data))  # выкинем пустые ключевые слова
    except Exception as e:
        logging.error('Error occurred while reading from yandex csv file. '
                      'Details: {} '.format(e))
        return 0

    ya_row_cnt = len(ya_data)
    go_row_cnt = len(go_data)
    row_cnt = ya_row_cnt + go_row_cnt
    processed_row_cnt = 0
    err_row_cnt = 0

    ad_finders = {YandexAdFinder(): ya_data,
                  GoogleAdFinder(): go_data}

    print('Всего необходимо обработать {0} ключевых слов:'
          '\n\t- {1} из yandex direct;'
          '\n\t- {2} из google adwords.\n'.format(row_cnt, ya_row_cnt, go_row_cnt))

    for ad_finder, data in ad_finders.items():
        for (key_word, title) in data:
            try:
                print('Обработка ключевого слова "{}"..'.format(key_word))
                ad_finder.set_attrs(key_word, title)
                success = ad_finder.find_ad()
                if not success:
                    err_row_cnt += 1
                    print('Рекламный блок для ключевого слова "{0}" не найден. '
                          'Был использован заголовок "{1}"'.format(key_word,
                                                                   title.encode("windows-1251")))
            except Exception as e:
                print('Во время обработки данного ключевого слова '
                      'произошла ошибка, оно будет пропущено. '
                      'Больше информации ищи в {}'.format(LOG_FILE_NAME))
                logging.error('Error occurred while processing '
                              'keyword. Details: {}'.format(e))
                err_row_cnt += 1
                continue
            finally:
                processed_row_cnt += 1
                print('Завершено {:.2%}\n'.format((processed_row_cnt / row_cnt)))

    print('Обработка всех ключевых слов завершена. '
          'Рекламный блок не найден для {} элементов.\n'.format(err_row_cnt))

    print('Нажмите любую клавишу для завершения.')
    input()

if __name__ == '__main__':
    main()
